package com.example.anotherpointlessapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import android.R.menu
import android.content.Intent









class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(findViewById(R.id.pointlessToolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val adapter = PointlessViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(AlpakaOne() , " Alpaka 1 ")
        adapter.addFragment(AlpakaTwo() , " Alpaka 2 ")
        adapter.addFragment(AlpakaThree() , " Alpaka 3 ")
        viewPager.adapter = adapter
        tabFrag.setupWithViewPager(viewPager)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.mymenu,menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {

        menu?.getItem(1)?.setEnabled(false)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_about -> {
            val intent1 = Intent(this, About::class.java)
            this.startActivity(intent1)
            true
        }
        R.id.action_testo -> {
            // User chose the "Settings" item, show the app settings UI...
            true
        }



        else -> {

            super.onOptionsItemSelected(item)
        }
    }

    class PointlessViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager){

        private val fragmentList : MutableList<Fragment> = ArrayList()
        private val titleList : MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment,title:String){
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }

    }
}
