package com.example.anotherpointlessapp


import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AlpakaThree : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_alpaka_three, container, false)



    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enableDialog()
    }
   fun enableDialog() {
       val newFragment = PointlessDialogFragment()
      newFragment.show(childFragmentManager, "pointlessDialog")
    }

    class PointlessDialogFragment : DialogFragment() {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            return activity?.let {
                // Use the Builder class for convenient dialog construction
                val builder = AlertDialog.Builder(it)
                builder.setMessage(R.string.dialog_alpaka)

                    .setPositiveButton(R.string.nigerun,
                        DialogInterface.OnClickListener { dialog, id ->

                            System.exit(0)

                        })
                    .setNegativeButton(R.string.back,
                        DialogInterface.OnClickListener { dialog, id ->
                            Toast.makeText(activity, "Nothing Happened", Toast.LENGTH_SHORT).show()
                        })
                // Create the AlertDialog object and return it
                builder.create()

            } ?: throw IllegalStateException("Activity cannot be null")
        }

    }}



