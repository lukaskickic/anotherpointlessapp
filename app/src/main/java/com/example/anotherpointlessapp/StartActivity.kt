package com.example.anotherpointlessapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class StartActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
    }
    fun runAlpaka(item: View)
    {
        val intent1 = Intent(this, MainActivity::class.java)
        this.startActivity(intent1)
        //finishAffinity()
    }

    fun runGAlpaka(item: View)
    {
        val intent2 = Intent(this, GlideActivity::class.java)
        this.startActivity(intent2)
        finishAffinity()
    }
}
